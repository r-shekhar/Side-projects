FROM  jupyter/datascience-notebook:latest

RUN conda install --quiet --yes -c conda-forge \
    'tensorflow=1.14*' \
    'keras=2.2*' \
    'tensorflow-probability' \
    'tensorflow-estimator'

RUN conda install -c r --quiet --yes \
	r-rstan r-forecast

RUN conda clean --all -f -y && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

RUN R -e "install.packages('bsts', repos='http://cran.us.r-project.org')"